﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FillwordsConsoleUI
{
    class Program
    {
        private const int Height = 30;
        private const int Width = 100;
        private const ConsoleColor DefaultColor = ConsoleColor.Yellow;

        private static List<ConsoleMenuElement> _menuItems = new List<ConsoleMenuElement>
        {
            new ConsoleMenuElement
            {
                Position = 13,
                Name = "New Game"
            },
            new ConsoleMenuElement
            {
                Position = 14,
                Name = "Continue"
            },
            new ConsoleMenuElement
            {
                Position = 15,
                Name = "Rating"
            },
            new ConsoleMenuElement
            {
                Position = 16,
                Name = "Exit"
            }
        };

        static void Main(string[] args)
        {
            SetConsoleSettings();
            RenderGame();
        }

        private static void RenderGame()
        {
            ConsoleKeyInfo input;
            var cursorMinPosition = _menuItems.First().Position;
            var cursorMaxPosition = _menuItems.Last().Position;
            var cursorCurrentPosition = cursorMinPosition;

            do
            {
                Console.SetCursorPosition(0, cursorCurrentPosition);
                input = Console.ReadKey();

                if (input.Key == ConsoleKey.UpArrow || input.Key == ConsoleKey.W)
                {
                    cursorCurrentPosition--;
                }

                if (input.Key == ConsoleKey.DownArrow || input.Key == ConsoleKey.S)
                {
                    cursorCurrentPosition++;
                }

                if (cursorCurrentPosition < cursorMinPosition)
                {
                    cursorCurrentPosition = cursorMaxPosition;
                }

                if (cursorCurrentPosition > cursorMaxPosition)
                {
                    cursorCurrentPosition = cursorMinPosition;
                }

                RenderContent(cursorCurrentPosition);

            } while (input.Key != ConsoleKey.Escape);
        }

        private static void RenderContent(int selectedVerticallyPosition)
        {
            const int topOffset = 8;
            const int offsetBetweenHeaderAndMenu = 2;

            Console.Clear();
            RenderConsoleOffset(topOffset);
            RenderHeader();
            RenderConsoleOffset(offsetBetweenHeaderAndMenu);
            RenderUserMenu(selectedVerticallyPosition);
        }

        private static void RenderHeader()
        {
            const string headerName = "Fill-words";

            RenderLineWithColor(headerName, DefaultColor);
        }

        private static void RenderUserMenu(int selectedVerticallyPosition)
        {
            foreach (var item in _menuItems)
            {
                var name = item.Name;
                var positionColor = item.Position == selectedVerticallyPosition 
                    ? ConsoleColor.DarkRed 
                    : DefaultColor;

                RenderLineWithColor(name, positionColor);
            }
        }

        private static void SetConsoleSettings()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.CursorVisible = false;
            Console.SetWindowSize(Width, Height);
            Console.SetBufferSize(Width, Height);
        }

        private static string GetElementHorizontallyCentered(string element)
        {
            var indent = new string(' ', (Width - element.Length) / 2);

            return $"{indent}{element}{indent}";
        }

        private static void RenderLineWithColor(string line, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(GetElementHorizontallyCentered(line));
        }

        private static void RenderConsoleOffset(int offset)
        {
            Console.WriteLine(new string('\n', offset));
        }
    }
}
