﻿namespace FillwordsConsoleUI
{
    public class ConsoleMenuElement
    {
        public int Position { get; set; }

        public string Name { get; set; }
    }
}
